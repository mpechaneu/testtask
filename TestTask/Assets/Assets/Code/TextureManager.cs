﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Code
{
    public class TextureManager: MonoBehaviour
    {
        private const float AlphaBorder = 0.1f;

        public Texture2D ImageTexture;
        public Camera Camera;

        private List<Vector2> _texturePoints;
        private Vector2 _managerPoint;

        public void Initialize()
        {
            _managerPoint = Camera.WorldToScreenPoint(transform.position);
            // texture should be in the center of the screen
            _managerPoint = _managerPoint - new Vector2((float)ImageTexture.width/2, (float)ImageTexture.height/2);
            InitializePoints();
            ShufflePoints();
        }

        private void InitializePoints()
        {
            _texturePoints = new List<Vector2>();
            int i, k;
            for (i = 0; i < ImageTexture.width; i++)
            {
                for (k = 0; k < ImageTexture.height; k++)
                {
                    var color = ImageTexture.GetPixel(i, k);
                    if (color.a > AlphaBorder)
                    {
                        _texturePoints.Add(_managerPoint + new Vector2(ImageTexture.width - i - 1, ImageTexture.height - k - 1));
                    }
                }
            }
        }

        private void ShufflePoints()
        {
            int i;
            for (i = 0; i < _texturePoints.Count; i++)
            {
                var swapIndex = _texturePoints.Count - i - 1;
                var randomIndex = Random.Range(0, _texturePoints.Count - i - 1);
                var temp = _texturePoints[randomIndex];
                _texturePoints[randomIndex] = _texturePoints[swapIndex];
                _texturePoints[swapIndex] = temp;
            }
        }

        public int GetPointsCount()
        {
            return _texturePoints.Count;
        }

        public List<Vector2> GetPoints()
        {
            return _texturePoints;
        }
    }
}
