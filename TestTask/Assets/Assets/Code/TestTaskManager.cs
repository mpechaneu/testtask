﻿using UnityEngine;

namespace Assets.Code
{
    public class TestTaskManager : MonoBehaviour
    {
        public TextureManager TextureManager;
        public ParticleManager ParticleManager;

        void Start()
        {
            TextureManager.Initialize();
            ParticleManager.Initialize(TextureManager.GetPointsCount());
            ParticleManager.InitializeTargetPoints(TextureManager.GetPoints());
        }
    }
}
