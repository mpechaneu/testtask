﻿using UnityEngine;

namespace Assets.Code
{
    public struct ParticleStruct
    {
        public Vector2 StartPosition;
        public Vector2 TargetPosition;
    }
}
