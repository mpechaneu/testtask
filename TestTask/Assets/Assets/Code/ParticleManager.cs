﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Code
{
    public class ParticleManager: MonoBehaviour
    {
        private const float FinishTime = 1;

        public Texture2D ParticleTexture;
        public Camera Camera;
        public int ParticleCount = 100000;
        [Range(0.001f,0.01f)]
        public float Speed = 0.001f;

        private float _animationTime = 0;
        private ParticleStruct[] _particles;

        public void Initialize(int pixelCount)
        {
            // if particles > pixel we decrease ParticleCount
            _particles = new ParticleStruct[Math.Min(pixelCount,ParticleCount)];
            InitializeParticles();
        }

        private void InitializeParticles()
        {
            int i;
            var halfParticleWidth = ParticleTexture.width / 2;
            var halfParticleHeight = ParticleTexture.height / 2;
            for (i = 0; i < _particles.Length; i++)
            {
                var xPosition = Random.value * Camera.pixelWidth;
                var yPosition = Random.value * Camera.pixelHeight;
                _particles[i].StartPosition.x = xPosition - halfParticleWidth;
                _particles[i].StartPosition.y = yPosition - halfParticleHeight;
            }
        }

        void OnGUI()
        {
            if (_animationTime < FinishTime)
            {
                _animationTime += Speed;
            }
            DrawParticles();
        }

        private void DrawParticles()
        {
            int i;
            Rect rect = new Rect();
            for (i = 0; i < _particles.Length; i++)
            {
                Vector2 drawPosition = Vector3.Slerp(_particles[i].StartPosition, _particles[i].TargetPosition, _animationTime);
                rect.x = drawPosition.x;
                rect.y = drawPosition.y;
                rect.width = ParticleTexture.width;
                rect.height = ParticleTexture.height;
                GUI.DrawTexture(rect, ParticleTexture);
            }
        }

        public void InitializeTargetPoints(List<Vector2> points)
        {
            int i;
            for (i = 0; i < _particles.Length; i++)
            {
                _particles[i].TargetPosition = new Vector2(points[i].x, points[i].y);
            }
        }
    }
}

